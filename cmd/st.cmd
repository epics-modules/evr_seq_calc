#!/usr/bin/env iocsh.bash
#require mrfioc2
#require evr_timestamp_buffer
require evr_seq_calc,dev

# -----------------------------------------------------------------------------
# Environment settings
# -----------------------------------------------------------------------------

epicsEnvSet("TOP", "$(E3_CMD_TOP)/..")

#iocshLoad("./iocsh/env-init.iocsh")
iocshLoad("$(mrfioc2_DIR)/env-init.iocsh")

epicsEnvSet("SYS",          "Mytest-ChpSy2:")
epicsEnvSet("EVR",          "EVR-001")
epicsEnvSet("DEV",          "Ctrl-$(EVR)")
epicsEnvSet("P",            "$(SYS)$(DEV)")
epicsEnvSet("PCIID",        "5:0.0")
epicsEnvSet("CHOP_SYS",     "$(SYS)")
epicsEnvSet("CHOP_EVR",     "$(P):")
epicsEnvSet("CHOP_DRV1",    "Chop-BWC-101:")
epicsEnvSet("CHOP_DRV2",    "Chop-BWC-102:")
epicsEnvSet("CHOP_DRV3",    "Chop-NA-101:")
epicsEnvSet("CHOP_DRV4",    "Chop-NA-201:")
epicsEnvSet("BASEEVTNO",    "150")
epicsEnvSet("TRIGEVT",      "14")
epicsEnvSet("SAMPLESIZE",   "1000")
epicsEnvSet("MAXBUF",       "10000")
epicsEnvSet("DEV",          "EVR")
epicsEnvSet("SYS",          "P")
epicsEnvSet("CHOP_DRV1",    "R:")
epicsEnvSet("MAXTSWF1",      "3") # Maximum_chopper_speed/14 + 2
epicsEnvSet("MAXTSWF2",      "4") # Maximum_chopper_speed/14 + 2
epicsEnvSet("MAXTSWF3",      "5") # Maximum_chopper_speed/14 + 2
epicsEnvSet("MAXTSWF4",      "6") # Maximum_chopper_speed/14 + 2


iocshLoad("$(evr_seq_calc_DIR)/evr_seq_calc2.iocsh", "P=$(CHOP_SYS), R1=$(CHOP_DRV1), R2=$(CHOP_DRV2), R3=$(CHOP_DRV3), R4=$(CHOP_DRV4), CHOP_EVR=$(CHOP_EVR), EVR=$(EVR), BASEEVTNO=$(BASEEVTNO), TRIGEVT=$(TRIGEVT), MAXBUF=$(MAXBUF), SAMPLESIZE=$(SAMPLESIZE), MAXTSWF1=$(MAXTSWF1), MAXTSWF2=$(MAXTSWF2), MAXTSWF3=$(MAXTSWF3), MAXTSWF4=$(MAXTSWF4)")

iocInit()

#