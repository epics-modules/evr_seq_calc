import time
import numpy as np
from p4p.client.thread import Context
from scipy.stats import skew
import pytest

# Base name for PVs
essname = "P:R1:"

# Define the PV names using string formatting for clarity and flexibility
pvs = {
    "ts_input": f"{essname}00-TS-I",
    "reset_ts_input": f"{essname}04-TS-I",
    "diff_ts": f"{essname}DiffTS",
    "diff_ts_samples": f"{essname}DiffTSSamples",
    "diff_ts_mean": f"{essname}DiffTSMean",
    "diff_ts_stddev": f"{essname}DiffTSStdDev",
    "diff_ts_skewness": f"{essname}DiffTSSkw",
    "diff_ts_max": f"{essname}DiffTSMax",
    "diff_ts_min": f"{essname}DiffTSMin",
    "sample_size": f"{essname}SampleSize-SP",
    "period": f"{essname}Period",
    "counter": f"{essname}SampleCounter",
    "mv_win_sp": f"{essname}MvWin-SP",
    "mv_win": f"{essname}MvWin"
}

def initialize_test_pvs(ctx, pvs):
    """
    Initialize common test PVs to their default values.
    """
    ctx.put(pvs["reset_ts_input"], ["0"])
    ctx.put(pvs["sample_size"], 5)
    ctx.put(pvs["period"], 71428568)
    ctx.put(pvs["counter"], 0)

# Test for setting MvWin-SP to sample_size + 1 and checking it is reset to sample_size
def test_mvwin_sp_set_and_check():
    # Set up context within the test
    print("Input Values:")
    with Context('pva') as ctx:
        initialize_test_pvs(ctx, pvs)
        time.sleep(0.1)

        # Get the current sample size
        sample_size = ctx.get(pvs["sample_size"])

        # Set the MvWin-SP PV to sample_size + 1
        ctx.put(pvs["mv_win_sp"], sample_size + 1)
        time.sleep(0.1)  # Allow the system to process

        # Check that MvWin-SP is set back to sample_size
        result = ctx.get(pvs["mv_win"])
        assert result == sample_size, f"MvWin should be set to {sample_size}, but got {result}"

def test_lower_sample_sp_below_mvwin_check():
    # Set up context within the test
    print("Input Values:")
    with Context('pva') as ctx:
        initialize_test_pvs(ctx, pvs)
        time.sleep(0.1)

        sample_size = ctx.get(pvs["sample_size"])
        win_size = ctx.get(pvs["mv_win"])
        assert win_size == sample_size, f"MvWin should be set to {sample_size}, but got {win_size}"

        ctx.put(pvs["sample_size"], 2)

        time.sleep(0.1)  # Allow the system to process
        sample_size = ctx.get(pvs["sample_size"])
        win_size = ctx.get(pvs["mv_win"])

        assert win_size == sample_size, f"MvWin should be set to {sample_size}, but got {win_size}"

# Define shared test parameters without using a fixture
test_params = [
   {
       "input_values": [71428565, 71428567, 1, 3, 2],
       "expected_diff_ts": [3, 1, -1, -3, -2]
   }
   ,
    {
        "input_values": [71428566, 71428567, 1, 3, [2, 71428565]],
        "expected_diff_ts": [2, 1, -1, -3, [-2, 3]]
    }
   ,
   {
       "input_values": [71428564, 71428566, 1, 3],
      "expected_diff_ts": [4, 2, -1,-3]
   }
]

# Function to check PV results
def check_pv(ctx, expected_pv, expected_value, atol=1e-5):
    """
    Generalized function to compare expected values to PV values.
    Handles both scalar and array-like expected values.
    """
    result = ctx.get(expected_pv)
    print(f"Debug: {expected_pv} result = {result}, expected = {expected_value}")
    
    if hasattr(expected_value, "__iter__"):
        assert isinstance(result, (list, np.ndarray)), f"Result for {expected_pv} should be a list/array, got {type(result)}"
        assert len(result) == len(expected_value), f"Length mismatch for {expected_pv}: got {len(result)}, expected {len(expected_value)}"
        
        for i, (res, exp) in enumerate(zip(result, expected_value)):
            if hasattr(exp, "__iter__"):  # Nested list
                assert np.array_equal(res, exp), f"Mismatch at index {i} for {expected_pv}: got {res}, expected {exp}"
            else:  # Scalar value
                assert np.isclose(res, exp, atol=atol), f"Failed at index {i} for {expected_pv}: got {res}, expected {exp}"
    else:
        # Scalar comparison
        assert np.isclose(result, expected_value, atol=atol), f"Failed: {expected_pv} got {result}, expected {expected_value}"


#Test to check PVs using shared parameters
@pytest.mark.parametrize("test_data", test_params)
def test_pvput_and_check(test_data):
    """
    Test putting input values to a PV and checking the corresponding output.
    """
    with Context('pva') as ctx:
        initialize_test_pvs(ctx, pvs)
        time.sleep(0.1)

        input_values = test_data["input_values"]
        expected_diff_ts = test_data["expected_diff_ts"]

        # Step through input values and perform checks
        for i, value in enumerate(input_values):
            # If value is a nested list, send it directly; otherwise, wrap it in a list
            value_to_put = list(value) if isinstance(value, (list, tuple)) else [value]
            ctx.put(pvs["ts_input"], value_to_put)
            time.sleep(0.1)  # Allow time for processing
            
            # Check the output PV against the expected value
            check_pv(ctx, pvs["diff_ts"], expected_diff_ts[i])

            
# Function to calculate statistics
def calculate_statistics(values):
    values_np = np.array(values)
    return {
        "mean": np.mean(values_np),
        "stddev": np.std(values_np, ddof=1),
        "skewness": skew(values_np, bias=False),
        "max": np.max(values_np),
        "min": np.min(values_np)
    }


# Test for final PVs using shared parameters
final_test_params = [
    {
        "input_values": [71428565, 71428567, 71428566, 1, 3],
        "expected_diff_ts": [3, 1, 2, -1, -3]
    }
]

@pytest.mark.parametrize("test_data", final_test_params)
def test_final_pvs(test_data):
    with Context('pva') as ctx:
        initialize_test_pvs(ctx, pvs)
        time.sleep(0.2)  # Allow full reset before processing test data

        expected_diff_ts = test_data["expected_diff_ts"]
        print("Expected Diff TS:", expected_diff_ts)
        input_values = test_data["input_values"]

        for i, value in enumerate(input_values):
            value_to_put = list(value) if isinstance(value, (list, tuple)) else [value]
            ctx.put(pvs["ts_input"], value_to_put)
            time.sleep(0.1)
            check_pv(ctx, pvs["diff_ts"], expected_diff_ts[i])        

        statistics = calculate_statistics(expected_diff_ts)
        time.sleep(0.2)
        
        actual_diff_ts = ctx.get(pvs["diff_ts_samples"])
        print("Expected Diff TS:", expected_diff_ts)
        print("Actual Diff TS:", actual_diff_ts)

        # Assertion with a tolerance
        assert np.array_equal(actual_diff_ts, expected_diff_ts), f"Samples mismatch"
        assert np.isclose(ctx.get(pvs["diff_ts_mean"]), statistics["mean"], atol=1e-5), f"Mean mismatch"
        assert np.isclose(ctx.get(pvs["diff_ts_stddev"]), statistics["stddev"], atol=1e-5), f"StdDev mismatch"
        assert np.isclose(ctx.get(pvs["diff_ts_skewness"]), statistics["skewness"], atol=1e-5), f"Skewness mismatch"
        assert ctx.get(pvs["diff_ts_max"]) == statistics["max"], f"Max value mismatch"
        assert ctx.get(pvs["diff_ts_min"]) == statistics["min"], f"Min value mismatch"