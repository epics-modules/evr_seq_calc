#!/usr/bin/env iocsh.bash
#require mrfioc2
require evr_seq_calc,dev

# -----------------------------------------------------------------------------
# Environment settings
# -----------------------------------------------------------------------------

epicsEnvSet("TOP", "$(E3_CMD_TOP)/..")

#iocshLoad("./iocsh/env-init.iocsh")
iocshLoad("$(mrfioc2_DIR)/env-init.iocsh")

epicsEnvSet("SYS",          "P:")
epicsEnvSet("EVR",          "EVR-001")
epicsEnvSet("DEV",          "Ctrl-$(EVR)")
epicsEnvSet("P",            "$(SYS)$(DEV)")
epicsEnvSet("PCIID",        "5:0.0")
epicsEnvSet("CHOP_SYS",     "$(SYS)")
epicsEnvSet("CHOP_EVR",     "$(P):")
epicsEnvSet("CHOP_DRV1",    "R1:")
epicsEnvSet("CHOP_DRV2",    "R2:")
epicsEnvSet("CHOP_DRV3",    "R3:")
epicsEnvSet("CHOP_DRV4",    "R4:")
epicsEnvSet("BASEEVTNO",    "150")
epicsEnvSet("TRIGEVT",      "14")
epicsEnvSet("SAMPLESIZE",   "6")
epicsEnvSet("MAXBUF",       "$(SAMPLESIZE)")
epicsEnvSet("MAXTSWF1",     "3") # Maximum_chopper_speed/14 + 2
epicsEnvSet("MAXTSWF2",     "4") # Maximum_chopper_speed/14 + 2
epicsEnvSet("MAXTSWF3",     "5") # Maximum_chopper_speed/14 + 2
epicsEnvSet("MAXTSWF4",     "6") # Maximum_chopper_speed/14 + 2
epicsEnvSet("DEV",          "EVR")

dbLoadRecords("test/chopper_evr.db","P=$(SYS), R1=$(CHOP_DRV1), R2=$(CHOP_DRV2), R3=$(CHOP_DRV3), R4=$(CHOP_DRV4), EVR=$(DEV), MAXTSWF1=$(MAXTSWF1), MAXTSWF2=$(MAXTSWF2), MAXTSWF3=$(MAXTSWF3), MAXTSWF4=$(MAXTSWF4)")

iocshLoad("$(evr_seq_calc_DIR)/evr_seq_calc4.iocsh", "P=$(CHOP_SYS), R1=$(CHOP_DRV1), R2=$(CHOP_DRV2), R3=$(CHOP_DRV3), R4=$(CHOP_DRV4), CHOP_EVR=$(CHOP_EVR), EVR=$(EVR), BASEEVTNO=$(BASEEVTNO), TRIGEVT=$(TRIGEVT), MAXBUF=$(MAXBUF), SAMPLESIZE=$(SAMPLESIZE), MAXTSWF1=$(MAXTSWF1), MAXTSWF2=$(MAXTSWF2), MAXTSWF3=$(MAXTSWF3), MAXTSWF4=$(MAXTSWF4)")

iocInit()
dbpf P:R1:Frq-S 14.0
#