# evr_seq_calc

European Spallation Source ERIC Site-specific EPICS module: evr_seq_calc

This module 
* calculates and loads the EVR sequencer events for neutron choppers
* analyse the timestamps of a neutron chopper and provide stats about the performance

Additonal information:
* [Release notes](RELEASE.md)
* [Building and Testing](https://confluence.esss.lu.se/display/IS/1.+Development+locally+with+e3+and+Conda#id-1.Developmentlocallywithe3andConda-BuildanEPICSmoduleandtestitlocally)
