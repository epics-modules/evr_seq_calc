#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <string.h> // Provides memcpy prototype
#include <stdlib.h> // Provides calloc prototype
#include <stdio.h>
#include <math.h>

int check_num_of_events(double *freqs, int arr_len, double base_event_no, double RF_freq, double base_freq, double sequence_freq, int *tick_list, int *event_list) {
    int i, j, k, total_events, num_of_events;
    k = 0;

    for(i = 0; i < arr_len; i = i + 1) {
        if (freqs[i] > 0) {
            num_of_events = (int) round(freqs[i]/sequence_freq);
            for(j = 0; j < num_of_events; j = j + 1) {
                k = k + 1;
            }
        }
    }

    total_events = k;
    return total_events;
}


int create_tick_event_list(double *freqs, int arr_len, double base_event_no, double RF_freq, double base_freq, double sequence_freq, int *tick_list, int *event_list) {
    int i, j, k, total_events, num_of_events;
    double ticks_per_event, ticks_per_cycle;
    ticks_per_cycle = round(RF_freq / base_freq* 1000000);
    k = 0;

    for(i = 0; i < arr_len; i = i + 1) {
        if (freqs[i] > 0) {
            ticks_per_event = ticks_per_cycle * base_freq / freqs[i];
            num_of_events = (int) round(freqs[i]/sequence_freq);
            for(j = 0; j < num_of_events; j = j + 1) {
                event_list[k] = ((int) base_event_no) + i;
                tick_list[k] =  (int) round(j * ticks_per_event);
                k = k + 1;
            }
        }
    }

    total_events = k;
    return total_events;
}

void apply_delay(double RF_freq, double base_freq, double base_event_no, double sequence_freq, int arr_len, int *delay_list, int *tick_list, int *event_list, double in_end_event_ticks) {
    int i, delay_index;
    double seq_len;
    seq_len = round(RF_freq / sequence_freq * 1000000);
    delay_index = 0;
    for (i = 0; i < arr_len; i = i + 1) {
        // find the current event delay element and apply
        delay_index = event_list[i]-base_event_no;
        tick_list[i] = tick_list[i]+delay_list[delay_index];
        if (tick_list[i] >= (int) (seq_len - in_end_event_ticks) && tick_list[i] <= (int)seq_len) {
            tick_list[i] = 0;
        }
        else if (tick_list[i] > seq_len) {
            tick_list[i] = tick_list[i] % (int) seq_len;
        }
    }   
    return ;
}

void sort_sequence(int arr_len, int *tick_list, int *event_list) {
    int i, j, tmp_tick, tmp_event;
    //Bubble sort   
    for (i = 0; i < arr_len - 1; i = i +1) {
        for (j = 0; j < arr_len - i - 1; j = j +1) {
            if (tick_list[j]> tick_list[j+1]) {
                tmp_tick = tick_list[j];
                tmp_event = event_list[j];
                tick_list[j] = tick_list[j+1];
                event_list[j] = event_list[j+1];
                tick_list[j+1] = tmp_tick;
                event_list[j+1] = tmp_event;
            }
        }
    }
    for (i = 0; i < arr_len; i = i + 1) {
    }
}

void resolve_conflicts(double RF_freq, double base_freq, double sequence_freq, int arr_len, int *tick_list, int *event_list, double in_end_event_ticks) {
    int i;
    double seq_len;
    seq_len =round(RF_freq / sequence_freq * 1000000);
    i = arr_len;
    //Search backwards to have the events being arranged in an event number increasing order
    while (i > 0) {
        if (tick_list[i-1] == tick_list[i]) {
            tick_list[i] = tick_list[i] + 1;
            if (tick_list[i] > (int) (seq_len - in_end_event_ticks)) {
                tick_list[i] = 0;
            }
            sort_sequence(arr_len, tick_list, event_list);
            i = arr_len ;
        }
        i = i - 1;
    }
    return ;
}
static int evr_sequence_calc(aSubRecord *precord) {
    
    double in_freqs[4], in_delays_ns[4], in_base_event_no, in_RF_freq, in_base_freq, sequence_freq, lower_seq_freq, end_event_number, in_end_event_ticks;
    int delays_ticks[4], full_tick_list[2048], full_event_list[2048];
    int i, arr_len, total_events;
    lower_seq_freq = 12.0;
    end_event_number = 127.0;
    
    in_freqs[0]         = fabsf(*(double *)precord->a);
    in_freqs[1]         = fabsf(*(double *)precord->b);
    in_freqs[2]         = fabsf(*(double *)precord->c);
    in_freqs[3]         = fabsf(*(double *)precord->d); 
    in_delays_ns[0]     = fabs(*(double *)precord->e);
    in_delays_ns[1]     = fabs(*(double *)precord->f);
    in_delays_ns[2]     = fabs(*(double *)precord->g);
    in_delays_ns[3]     = fabs(*(double *)precord->h);
    in_base_event_no    = fabsf(*(double *)precord->i);
    in_RF_freq          = fabsf(*(double *)precord->j); //MHz
    in_base_freq        = fabsf(*(double *)precord->k);
    in_end_event_ticks  = fabsf(*(double *)precord->l + 1.0); //Add 1, arrays starts at 0
    

    //maximum event number for the MRF EVR is 255, base event maximum is 252, since four concecutive events are used
    if (in_base_event_no > 252 || in_base_event_no < 0) {
        return 2;
    }

    //maximum end event ticks are the RF frequency, number of ticks
    if (in_end_event_ticks > in_RF_freq*1e6/in_base_freq) {
        return 3;
    }
    
    for (i = 0; i < sizeof(in_freqs)/sizeof(in_freqs[0]); i = i +1) {
        //Convert ns to us by dividing by 1000, then multiply with MHz
        delays_ticks[i] = (int) round(in_delays_ns[i] / 1000.0 * in_RF_freq);
    }   
       
    arr_len = sizeof(in_freqs) / sizeof(in_freqs[0]);

    sequence_freq = in_base_freq;
    for (i = 0; i < arr_len; i = i + 1) {
        if ( sequence_freq > in_freqs[i] && in_freqs[i] > 0.0) {
            sequence_freq = in_base_freq/lower_seq_freq;
        }
    }

    //maximum number of events in the EVR sequencer are 2048, if too many, then exit
    if (check_num_of_events(in_freqs, arr_len, in_base_event_no, in_RF_freq, in_base_freq, sequence_freq, full_tick_list, full_event_list) > (2048-48)){
        return 1;
    }

    //Create two arrays, one with all events and one with the corresponding tick timing
    total_events = create_tick_event_list(in_freqs, arr_len, in_base_event_no, in_RF_freq, in_base_freq, sequence_freq, full_tick_list, full_event_list);


    //Reduce the array sizes and copy values
    int tick_list[total_events], event_list[total_events];
    //arr_len = sizeof(tick_list) / sizeof(tick_list[0]);
    for(i = 0; i < total_events; i = i + 1){
        tick_list[i] = full_tick_list[i];
        event_list[i] = full_event_list[i];
    }

    //Apply delay for each event in the tick array
    apply_delay(in_RF_freq, in_base_freq, in_base_event_no, sequence_freq, total_events, delays_ticks, tick_list, event_list, in_end_event_ticks);

    //Sort the list after tick size
    sort_sequence(total_events, tick_list, event_list);

    //Ensure no events happen on the same tick count
    resolve_conflicts(in_RF_freq, in_base_freq, sequence_freq,  total_events, tick_list, event_list, in_end_event_ticks);

    //Add one more event to allow for sequence-end-event 127
    int out_events[total_events+1];
    int  out_ticks[total_events+1];
    for (i = 0; i < total_events; i = i + 1) {
        out_events[i] = (int) event_list[i];
        out_ticks[i] = (int) tick_list[i];
    }

    //Add sequence end event, a few ticks before the end to allow the event to be processed
    arr_len = sizeof(out_ticks)/sizeof(out_ticks[0]);

    out_ticks[total_events] = round(in_RF_freq / sequence_freq * 1000000) - in_end_event_ticks;
    out_events[total_events] = end_event_number;
    
    //Output event list
    precord->neva = arr_len;    
    memcpy(precord->vala, out_events, arr_len * sizeof(out_events[0]));
    
    //Output tick list
    precord->nevb = arr_len;
    memcpy(precord->valb, out_ticks, arr_len * sizeof(out_ticks[0]));
    
    //Output commit value
    *(int *)precord->valc = 1;

return 0;
}

epicsRegisterFunction(evr_sequence_calc);

