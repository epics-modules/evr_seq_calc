#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

double calculate_mean(int data[], int size) {
    double sum = 0.0;
    for (int i = 0; i < size; i++) {
        sum += data[i];
    }
    return sum / size;
}

double calculate_stddev(int data[], int size, double mean) {
    double sum = 0.0;
    for (int i = 0; i < size; i++) {
        sum += pow((double)data[i] - mean, 2);
    }
    return sqrt(sum / (size-1));
}

double calculate_skewness(int data[], int size, double stddev, double mean) {    
    double skewness = 0.0;
    double dblSize = (double) size;
    if (size < 3) {
        return 0.0;
    }

    if (stddev == 0.0) {
        return 0.0;
    }

    for (int i = 0; i < size; i++) {
        double normalized_value = ((double)data[i] - mean) / stddev;
        double cubed_value = pow(normalized_value, 3);  
        skewness += cubed_value;
    }
    skewness = (dblSize) / ((dblSize - 1.0) * (dblSize - 2.0)) * skewness;
    return skewness;
}

static int tsanalysis(aSubRecord *precord) {
    int i;
    int tdcLen = precord->nea;
    int refLen = precord->neb;
    int tdcTS[tdcLen], refTS[tdcLen+1], diffTS[tdcLen];
    int sampleCounter = (*(int *)precord->d);
    int samplesLimit = (*(int *)precord->e);
    int moveWinLimit = (*(int *)precord->f);
    int moveWinCounter = (*(int *)precord->g);
    int bufLen = precord->noh;
    int diffTSAnalysis[bufLen];

    if (sampleCounter <= 0) {
        sampleCounter = 0;
    }
    memset(tdcTS, 0, sizeof(diffTS));
    memset(refTS, 0, sizeof(refTS));
    memset(diffTS, 0, sizeof(diffTS));
    memset(diffTSAnalysis, 0, sizeof(diffTSAnalysis));
    double skewness = 0.0;
    double mean = 0.0;
    double stddev = 0.0;
    int updateRecords = 0;
    int minDiff = 999999999;
    int maxDiff = -999999999;
    int period = 0;
    period = *(int *)precord->c;
    memcpy(tdcTS, precord->a, tdcLen * sizeof(tdcTS[0]));
    memcpy(refTS, precord->b, refLen * sizeof(refTS[0]));
    memcpy(diffTSAnalysis, precord->h, bufLen * sizeof(diffTSAnalysis[0]));
    if (updateRecords == 1){
        updateRecords = 0;
    }

    // If no TDC timestamps, do nothing
    if (tdcLen > 0) {
        // if TDC array is longer, add an element to the end of the ref array, TS one period later
        if (tdcLen > refLen) {
            refTS[tdcLen-1] = refTS[tdcLen-2] + period;
        } 
        // if first ref element timestamp is much smaller, add one period time to to all elements
        if ((refTS[0] - tdcTS[0]) < (-period/2)) {
            for (i = 0; i < tdcLen; i++) {
                refTS[i] = refTS[i] + period;
            }
        }
        //if first ref element timestamp is much larger than TDC, reduce one period
        if (refTS[0] - tdcTS[0] > (period/2)) {
            for (i = 0; i < tdcLen; i++) {
                refTS[i] = refTS[i] - period;
            }
        }
    }
  
    // Add latest timestamps difference to the full array
    for (i = 0; i < tdcLen; i++) {
        diffTS[i] = refTS[i] - tdcTS[i];
        diffTSAnalysis[sampleCounter] = diffTS[i]; 
        sampleCounter++;
        moveWinCounter++;
        
    }

    precord->neva = tdcLen;  
    memcpy(precord->vala, diffTS, tdcLen * sizeof(diffTS[0]));

    // Perform analysis if sample window size is reached
    if (moveWinCounter >= moveWinLimit) {
        mean = calculate_mean(diffTSAnalysis, samplesLimit);
        stddev = calculate_stddev(diffTSAnalysis, samplesLimit, mean);
        skewness = calculate_skewness(diffTSAnalysis, samplesLimit, stddev, mean);
        for (i = 0; i < samplesLimit; i++) {
            if (minDiff > diffTSAnalysis[i]) {
                minDiff = diffTSAnalysis[i];
            }
            if (maxDiff < diffTSAnalysis[i]) {
                maxDiff = diffTSAnalysis[i];
            }
        }
        precord->nevd = samplesLimit;  
        memcpy(precord->vald, diffTSAnalysis, samplesLimit * sizeof(diffTSAnalysis[0]));
        *(double *)precord->vale = mean;
        *(double *)precord->valf = stddev;
        *(double *)precord->valg = skewness;
        *(int *)precord->valh = maxDiff;
        *(int *)precord->vali = minDiff;
        moveWinCounter = moveWinCounter - moveWinLimit;
        updateRecords = 1;
    }

    if (sampleCounter >= samplesLimit){
        // Create array to transfer the timestamps that are more than sample size to next analysis
        int carryOverTS[sampleCounter - samplesLimit + 1];
        // Reset the carryOver array
        memset(carryOverTS, 0, sizeof(carryOverTS));
        //Reset sampleCounter to start counting up for the carry over timestamps
        if (sampleCounter == samplesLimit) {
            sampleCounter = 0;
        }
        // Save the events that are more than the allowed sample size, if any
        for (i = 0; i < (sampleCounter - samplesLimit); i++) {
            carryOverTS[i] = diffTSAnalysis[samplesLimit + i];
        }
        //Move the carry over timestamps to the fresh samples TS array
        for (i = 0; i < sampleCounter - samplesLimit + 1; i++) {
            diffTSAnalysis[i] = carryOverTS[i];
            sampleCounter = i + 1;
        }
    }

    precord->nevj = samplesLimit;
    memcpy(precord->valj, diffTSAnalysis, samplesLimit * sizeof(diffTSAnalysis[0]));
    *(int *)precord->valc = updateRecords;
    *(int *)precord->valb = sampleCounter;
    *(int *)precord->valk = moveWinCounter;

    return 0;
}

epicsRegisterFunction(tsanalysis);
